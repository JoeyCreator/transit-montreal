var cssRule =
    "color: black;" +
    "padding: 1rem;" +
    "font-size: 25px;" +
    "font-weight: sans-serif;" +
    "text-shadow: none;" +
    "border-radius: 15px;" +
    "background-color: 	#FFFF66;"
"filter: dropshadow(color=rgb(249, 162, 34), offx=1, offy=1);";
console.log(
    "%c Warning! Using this console may allow attackers to impersonate you and steal your information using an attack called Self-XSS.Do not enter or paste code that you do not understand.",
    cssRule
);