mapboxgl.accessToken =
  "pk.eyJ1Ijoiam9leWNyZWF0b3IiLCJhIjoiY2xzdzI2MnRhMDg2djJqbXV5OHpyazRyayJ9.CJr3KxbOru2GHX7Qf7SAsg";
const map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/dark-v10",
  center: [-96.052335, 39.159882],
  zoom: 3.5,
});

const apiKey =
  "5502d5b70741ddeb14dbec51b8e5347436c8379962020822d897c5ceb22102e7";

fetch(
  `https://external.transitapp.com/v3/public/stop_departures?api_key=${apiKey}`
)
  .then((response) => response.json())
  .then((data) => {
    // Use the data to add markers to the map
    data.forEach((departure) => {
      var marker = new mapboxgl.Marker({
        color: "blue", // Change the marker color as needed
      })
        .setLngLat([departure.longitude, departure.latitude])
        .setPopup(
          new mapboxgl.Popup().setHTML(
            `<h3>${departure.name}</h3><p>${departure.departure_time}</p>`
          )
        )
        .addTo(map);
    });
  })
  .catch((error) => console.error("Error fetching data:", error));
